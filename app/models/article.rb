class Article < ActiveRecord::Base
  validates :name, :store, presence: true
  belongs_to :store
end
