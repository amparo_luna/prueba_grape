module API
  module Services
    class Stores < Grape::API
      resource :stores do
        desc "Load all the stores that are stored in the Database."
        get do
          render Store.all, { meta: true, meta_key: :success }
        end
      end
    end
  end
end