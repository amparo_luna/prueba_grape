module API
  module Services
    class Articles < Grape::API
      resource :articles do
        
        desc "Load all the articles that are in the Database."
        get do
          render Article.all, { meta: true, meta_key: :success }
        end
        
        desc "Load all the articles from a specific store that are in the Database."
        params do
          requires :id, type: Integer, desc: "Store id."
        end
        get "stores/:id" do
          render Store.find(params[:id]).articles, { meta: true, meta_key: :success }
        end
      end
    end
  end
end