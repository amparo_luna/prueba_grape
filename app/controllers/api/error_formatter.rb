module API
  module ErrorFormatter
    def self.call message, backtrace, options, env
      if message.is_a?(Hash)
      	message.to_json
      else
      	{ success: false, error_code: options[:default_status], error_msg: message }.to_json
      end
    end
  end
end