module API
  class Base < Grape::API
    rescue_from :all
    
    format :json
    formatter :json, Grape::Formatter::ActiveModelSerializers
    error_formatter :json, API::ErrorFormatter    

    rescue_from Grape::Exceptions::ValidationErrors do |e|
      error_response(message: { success: false, error_msg: "Bad Request", error_code: 400 }, status: 400)
    end

    rescue_from ActiveRecord::RecordNotFound do |e|
      error_response(message: { success: false, error_msg: "Record Not Found", error_code: 404 }, status: 404)
    end

    before do
      error!({ success: false, error_msg: "Unauthorized", error_code: 401 }, 401) unless authenticated
    end

    helpers do
      def authenticated
        headers['Authorization'] == "Basic bXlfdXNlcjpteV9wYXNzd29yZA=="
      end
    end
   
    mount API::Services::Stores
    mount API::Services::Articles
  end
end