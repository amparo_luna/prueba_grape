
stores = ["Store - Colombia", "Store - Ecuador", "Store - Chile"]
stores.each do |name|
  Store.create(name: name, address: "123 St.")
end

Store.first.articles.create(name: "Shoes Ref. 1", description: "Nice Shoes", price: 35.5, total_in_shelf: 37.5, total_in_vault: 40)
Store.first.articles.create(name: "Shoes Ref. 2", description: "Nice Shoes", price: 35.5, total_in_shelf: 37.5, total_in_vault: 40)

Store.last.articles.create(name: "Shoes Ref. 3", description: "Nice Shoes", price: 35.5, total_in_shelf: 37.5, total_in_vault: 40)