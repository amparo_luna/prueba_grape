class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.string :name, null: false
      t.string :description
      t.decimal :price, precision: 10, scale: 2
      t.decimal :total_in_shelf, precision: 10, scale: 2
      t.decimal :total_in_vault, precision: 10, scale: 2
      t.belongs_to :store, index: true

      t.timestamps
    end
  end
end
