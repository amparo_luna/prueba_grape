shared_context 'a successfull request' do
  it "responds with 200 HTTP status" do
    expect(response).to have_http_status(:ok)
  end
  
  it "json includes \"success\" => true" do
    expect(json_response["success"]).to eq(true)
  end
end

shared_context 'an unsuccessfull request' do |status, status_symbol, msg|
  it "responds with #{status} HTTP status" do
    expect(response).to have_http_status(status_symbol)
  end

  it "json includes \"success\" => false" do
    expect(json_response["success"]).to eq(false)
  end

  it "json includes \"error_msg\" => \"#{msg}\"" do
    expect(json_response["error_msg"]).to eq(msg)
  end

  it "json includes \"error_code\" => #{status}" do
    expect(json_response["error_code"]).to eq(status)
  end
end