module DataHelper
  
  def create_all
    create_stores
    create_articles
  end
  
  def create_stores
    @store_without_articles  = create(:store)
    @store_with_articles_one = create(:store)
    @store_with_articles_two = create(:store)
  end
  
  def create_articles
    @article_one   = create(:article, store: @store_with_articles_one)
    @article_two   = create(:article, store: @store_with_articles_one)
    @article_three = create(:article, store: @store_with_articles_two)
  end
end