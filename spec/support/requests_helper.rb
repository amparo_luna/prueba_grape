module RequestsHelper
  def json_response
    @json ||= JSON.parse(response.body)
  end
  
  def http_auth_token
    @token ||= ActionController::HttpAuthentication::Basic.encode_credentials("my_user","my_password")
  end
end