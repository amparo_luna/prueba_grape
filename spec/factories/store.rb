FactoryGirl.define do
  sequence(:name) { |n| "Store No.#{n}" }
  factory :store do
    name
    address  "123 St."
  end
end