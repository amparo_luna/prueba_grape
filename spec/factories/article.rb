FactoryGirl.define do
  factory :article do
    sequence(:name) { |n| "Beautiful shoes No.#{n}" }
    description "Best shoes ever"
    price 45.5
    total_in_shelf 48.5
    total_in_vault 50
    association :store, factory: :store
  end
end

