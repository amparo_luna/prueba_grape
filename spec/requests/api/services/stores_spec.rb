require 'rails_helper'

describe "Stores (API Service)" do
  describe "GET /services/stores" do
    context "without HTTP Basic username and password" do
      before do
        get "/services/stores"
      end
      
      it_behaves_like 'an unsuccessfull request', 401, :unauthorized, "Unauthorized"
    end
    
    context "with HTTP Basic username and password" do
      describe "empty stores" do
        before do
          get "/services/stores", {}, HTTP_AUTHORIZATION: http_auth_token
        end
        
        it_behaves_like 'a successfull request'
        
        it "json includes \"stores\" => []" do
          expect(json_response["stores"]).to be_empty
        end
      end
      
      describe "one or more stores" do
        before do
          create_stores
          get "/services/stores", {}, HTTP_AUTHORIZATION: http_auth_token
        end
        
        it_behaves_like 'a successfull request'
        
        it "json includes stores key" do
          expect(json_response).to include("stores")
        end
        
        it "json[\"stores\"] contains all stores from the db" do
          expect(json_response["stores"][0]["name"]).to eq(@store_without_articles.name)
          expect(json_response["stores"][1]["name"]).to eq(@store_with_articles_one.name)
          expect(json_response["stores"][2]["name"]).to eq(@store_with_articles_two.name)
        end
      end
    end
  end
end