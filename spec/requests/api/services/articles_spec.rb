require 'rails_helper'

describe "Articles (API Service)" do
  describe "GET /services/articles/stores/:id" do
    before do
      create_all
    end
    
    context "without HTTP Basic username and password" do
      before do
        get "/services/articles/stores/#{@store_without_articles.id}"
      end
      
      it_behaves_like 'an unsuccessfull request', 401, :unauthorized, "Unauthorized"

    end
    
    context "with HTTP Basic username and password" do    
      context "with an invalid (no numeric) :id param" do
        before do
          get "/services/articles/stores/id", {}, HTTP_AUTHORIZATION: http_auth_token
        end
        
        it_behaves_like 'an unsuccessfull request', 400, :bad_request, "Bad Request"

      end
      
      context "with a valid (numeric) :id param" do
        context "when the store does not exist" do
          before do
            get "/services/articles/stores/10000", {}, HTTP_AUTHORIZATION: http_auth_token
          end
          
          it_behaves_like 'an unsuccessfull request', 404, :not_found, "Record Not Found"

        end
        
        context "when the store exists" do
          describe "store with no articles" do
            before do
              get "/services/articles/stores/#{@store_without_articles.id}", {}, HTTP_AUTHORIZATION: http_auth_token
            end
            
            it_behaves_like 'a successfull request'
            
            it "json includes empty articles key" do
              expect(json_response["articles"]).to be_empty
            end
            
          end
          
          describe "store with one or more articles" do
            before do
              get "/services/articles/stores/#{@store_with_articles_one.id}", {}, HTTP_AUTHORIZATION: http_auth_token
            end
            
            it_behaves_like 'a successfull request'
            
            it "json includes articles key" do
              expect(json_response).to include("articles")
            end
            
            it "lists all articles from the store" do
              expect(json_response["articles"][0]["name"]).to eq(@article_one.name)
              expect(json_response["articles"][1]["name"]).to eq(@article_two.name)
            end
            
            it "does not list articles from other stores" do
              expect(json_response["articles"].size).to eq(2)
            end
          end
        end
      end
    end
  end
  
  describe "GET /services/articles" do
    context "without HTTP Basic username and password" do
      before do
        get "/services/articles"
      end
      
      it_behaves_like 'an unsuccessfull request', 401, :unauthorized, "Unauthorized"
    end
    
    context "with HTTP Basic username and password" do
      describe "empty articles" do
        before do
          get "/services/articles", {}, HTTP_AUTHORIZATION: http_auth_token
        end
        
        it_behaves_like 'a successfull request'
        
        it "json includes \"articles\" => []" do
          expect(json_response["articles"]).to be_empty
        end
      end
      
      describe "one or more articles" do
        before do
          create_all
          get "/services/articles", {}, HTTP_AUTHORIZATION: http_auth_token
        end
        
        it_behaves_like 'a successfull request'
        
        it "json includes articles key" do
          expect(json_response).to include("articles")
        end
        
        it "json[\"articles\"] contains all articles from the db" do
          expect(json_response["articles"][0]["name"]).to eq(@article_one.name)
          expect(json_response["articles"][1]["name"]).to eq(@article_two.name)
          expect(json_response["articles"][2]["name"]).to eq(@article_three.name)
        end
      end
    end
  end
end